-- import required modules
import qualified Data.Text as DT


-- ********************** Q1 *******************************************
-- add two number and double result
-- param: a first number
-- param: b second number
-- return: sum of two number and then double the result
add_and_double:: (Num a)=> a -> a -> a
add_and_double a b = (a+b)*2

-- ********************** Q2 *******************************************
-- define operator *+
-- return: sum of two number and then double the result

(*+):: (Num a) => a -> a -> a
(*+) a b = a `add_and_double` b

-- alternative: Ln 10
-- (*+) = add_and_double

-- ********************** Q3 *******************************************
-- solve quardric equation using delta= b^2 - 4ac, x= (-b +- sqrt(delta))/ 2a, 
-- check if delta<0, then throw exception that there is no solution, 
-- more useful than not checking it, because if we dont check for it, 
-- it will return (NaN,NaN) when the equation has no solution, which is not much used mathematically
-- param: a coefficient attach to the power of 2
-- param: b coefficient attach to the power of 1
-- param: c coefficient attach to the power of 0
-- return: solution of ax^2 + bx+ c=0

solve_quardric_equation:: Double->Double->Double ->(Double,Double)
solve_quardric_equation a b c 
 |delta < 0 = error "No Solution"
 |otherwise =(((-b)+sqrt(delta))/(2*a),((-b)-sqrt(delta))/(2*a))
   where 
    delta = b^2 - 4*a*c

-- alternative: Ln 18
-- solve_quardric_equation a b c = let delta= b^2 - 4*a*c in (((b+sqrt(delta))/(2*a)),((b-sqrt(delta))/(2*a)))

-- ********************** Q4 *******************************************
--  first_n using take (in-built)
-- param n: number of numbers that is taken
-- return: list of number that are taken

first_n:: Int -> [Int]
first_n n
 |n<=0 = error "Cant take negative amount of number"
 |otherwise = take n [1..]

-- ********************** Q5 *******************************************
--  first_n_integers using take_integer, check to make sure the entered amount is not negative
-- param n: number of numbers that is taken
-- return: list of number that are taken

first_n_integers :: Integer -> [Integer]
first_n_integers n
 |n<0 = error "Cant define negative index"
 |otherwise = take_integer n [1..] 
   where
    take_integer _[] = error "Can find in empty list"
    take_integer a (x:xs)
	 | a<0 = error "Wrong Indexing"
	 | a==0 = []
	 | otherwise = x: take_integer (a-1) xs

-- ********************** Q6 *******************************************
--  double_factorial(n)= f0*f1*f2*..*fn, let will avoid the risk of having stack over flown, as noted in problems_logs.txt
-- param n: number that is used to take double the factorial of
-- return: (n!)(n-1)!...0!

double_factorial::Integer-> Integer
double_factorial n
 |n<0 =error "can perform factorial on negative number"
 |n==0 = 1
 |otherwise =
  let
    factorial n
	 |n<0 =error "cant perform factorial on negative number"
	 |n==0 =1
	 |otherwise =n * factorial (n-1)
  in factorial n * double_factorial (n-1)

-- ********************** Q7 *******************************************
-- define factorials list of factorials
-- 0!=1
-- return: [0!,1!,2!,...,k!,...]

factorials:: [Integer]
factorials = 1 : zipWith (*) factorials [1..]
-- ********************** Q8 *******************************************
--  isPrime function to check Integer, I used the approach map because map is very useful and optimized function, also, the number that may appear to be the divisors tend to be 2 3 4 5 8,.. which start from the left hand side of the list from 2 -> that number
-- param a: number to check 
-- return: boolean value saying that is a prime or not
isPrime:: Integer -> Bool
isPrime a
 | abs a<2 = False
 |otherwise = not (0 `elem` map ((abs a) `mod`) [2..((abs a)-1)])

-- ********************** Q9 *******************************************
-- define primes list of primes number, applying is Prime from previous
-- return: list of prime number from 1

primes :: [Integer]
primes = filter isPrime [1..]


-- ********************** Q10 *******************************************
--  sum' : sum a list of number recursively
-- param xs: list 
-- return: sum of the list

sum':: (Num a)=> [a] -> a
sum' [] = 0
sum' (x:xs) = x + sum' xs

-- ********************** Q11 *******************************************
--  sumL : sum a list of number using foldl
-- param xs: list 
-- return: sum of the list
sumL :: (Num b, Foldable t) => t b -> b
sumL xs = foldl (\acc x->acc+x) 0 xs

-- ********************** Q12 *******************************************
--  multR : multiply a list of number using foldr
-- param xs: list 
-- return: product of the list

multR :: (Num b, Foldable t) => t b -> b
multR xs = foldr (\acc x->x*acc) 1 xs

-- ********************** Q13 *******************************************
--  guess: take in a string, test if it matches "I love functioning programming", and a number, test if it <5, not exactly doing I/O or loop with IO
-- param str_inp: input string 
-- param num_inp: input number 
-- return: State of the game

guess :: (Num a, Ord a) => String -> a -> [Char]
guess str_inp num_inp 
 |num_inp <5 = if DT.toLower (DT.pack str_inp) == DT.pack "i love functional programming" then "yay! You have won" else "Enter it again"
 |otherwise = "You have Lost" 

-- ********************** Q14 *******************************************
--  function: dot_product: get the dot product of 2 vector,
 -- represent vector as list because in math, it make more sense if we represent two vectors 
 -- separately for calculation, then, a vector could have multiple dimension 
 -- and tuples are fixed length so cant
 -- do much about using this function for different vector having different number of dimensions
 -- also check for the size of the vector, and empty vector is not defined as 0 vector and if vectors dont have 
 -- same length, we cant do calculation
-- param xs: first vector
-- param ys: second vector 
-- return: a number, dot product of vector

dot_product:: (Num a)=> [a] -> [a] -> a
dot_product xs ys
 | length xs ==0 || length ys ==0 = error "empty vector detected"
 | length xs == length ys = sum (zipWith (*) xs ys)
 | otherwise = error "vectors size not match"
-- ********************** Q15 *******************************************
--  function: is_even: check if a number is even or not
-- param a : number to check
-- return : True if is even, False if is odd
is_even:: (Integral a) => a->Bool
is_even n
 | n `mod` 2 ==1 = False
 | otherwise = True


-- ********************** Q16 *******************************************
--  function: unixname: remove vowels in string
-- param str : raw string
-- return : new string with no vowel

unixname:: [Char] -> [Char]
unixname [] = []
unixname (x:xs)
 | x `elem` ['a','e','i','o','u','A','E','I','O','U'] = [] ++ unixname xs
 | otherwise = x: unixname xs


-- ********************** Q17 *******************************************
--  function: intersection: get the common element of 2 list
-- param xs : first list 
-- param ys : second list
-- return : the list of character that are the same in two list

-- intersection:: [a] -> [a] -> [a]

intersection :: (Eq a, Foldable t) => [a] -> t a -> [a]
intersection xs ys= filter (\x -> x `elem` ys) xs

-- ********************** Q18 *******************************************
--  function: censor: replace vowels with x
-- param str : raw string
-- return : new string with vowels replaced by 'x'

censor:: [Char] -> [Char] 
censor [] = []
censor (x:xs)
 | x `elem` ['a','e','i','o','u','A','E','I','O','U'] = 'x': censor xs
 | otherwise = x: censor xs

